﻿INSERT INTO instrument(instrument_name, instrument_producer_firm, instrument_price) VALUES
('Fender Jazz Bass','Fender',-15),
('Zildjan Drum Kit', 'Zildjan', -56);

INSERT INTO musician(musician_name, musician_surname, musician_age, musician_role_in_group,musician_instrument_id) VALUES
(NULL,'Denisov','vocalist',3),
('Andrew',NULL,'drummer',2),
('Sergey','Sek',10,'rhytm_guitarist',5),
('Kyrychenko','Ivan',NULL,4),
('Antony','Aksenov',NULL,1);

INSERT INTO music_label(label_name,group_quantity) VALUES
(NULL,NULL);

INSERT INTO band(group_name, group_genre, group_label_id, group_vocalist_id, group_lead_guitarist_id,
group_rhytm_guitarist_id, group_bass_guitarist_id, group_drummer_id) VALUES
('While Ashes Fade','Melodic Metalcore',18,41,24,33,45,27);

INSERT INTO album(album_name,album_release_date,track_quantity,album_group_id) VALUES
('Ashes',NULL,7,1);

INSERT INTO tour(tour_name,tour_start_date,tour_end_date,tour_group_id) VALUES
('Defiler_support','10/05/2011','12/05/2010',1);

INSERT INTO listeners(listener_quantity,average_age,listener_group_id) VALUES
(-5,9,1),
(5,-9,2);

INSERT INTO clip(clip_song, clip_release_date,clip_group_id) VALUES
('Mediocrity',NULL,1);