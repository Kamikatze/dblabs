﻿CREATE ROLE administrator WITH SUPERUSER CREATEDB CREATEROLE LOGIN;
CREATE ROLE moderator WITH CREATEROLE LOGIN;
CREATE ROLE username WITH LOGIN;
CREATE ROLE guest;

GRANT INSERT, SELECT, UPDATE ON ALL TABLES IN SCHEMA public TO moderator;

GRANT SELECT ON ALL TABLES IN SCHEMA public TO username;

GRANT SELECT (instrument_id, instrument_name, instrument_producer_firm) ON instrument TO guest;
GRANT SELECT (musician_name, musician_surname, musician_role_in_group, musician_instrument_id) ON musician TO guest;
GRANT SELECT (label_name) ON music_label TO guest;
GRANT SELECT ON band TO guest;
GRANT SELECT (album_name, album_group_id, track_quantity) ON album TO guest;
GRANT SELECT ON tour TO guest;
GRANT SELECT (clip_id, clip_song, clip_group_id) ON clip TO guest;