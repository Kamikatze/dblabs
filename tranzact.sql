﻿SELECT * FROM musician WHERE musician_age=17 ORDER BY musician_name; -- Найти семнадцатилетних музыкантов и отсортировать по имени 1

SELECT * FROM musician WHERE musician_age BETWEEN 16 AND 17 ORDER BY musician_age; -- Найти несовершеннолетних музыкантов и отсортировать по возрасту по возрастанию 2

SELECT * FROM musician WHERE musician_role_in_group='drummer' ORDER BY musician_surname; -- Найти барабанщиков и сортировать по фамилии 3

SELECT album_name, album_release_date FROM album WHERE album_release_date>'01/01/2010'; -- Вывести альбомы, которые выпущены позже 1го января 2010 года 4

SELECT musician_name, musician_surname, instrument_name, instrument_producer_firm, instrument_price FROM musician

INNER JOIN instrument ON musician.musician_instrument_id=instrument.instrument_id ORDER BY instrument_price DESC; -- Сделать отчет по музыкантам, и инструментам, которые им принадлежат. 
											-- Отсортировать по убыванию, чтобы легче было определить музыканта с самым дорогим инструментом.5

SELECT group_name, group_genre, average_age FROM band
RIGHT OUTER JOIN listeners ON band.group_id=listeners.listener_group_id; -- При удалении группы, слушатели остаются, и можно собрать статистику по среднему возрасту целевой аудитории слушателей группы 6

WITH musician_16 AS(SELECT * FROM musician WHERE musician_age=16),
     musician_17 AS(SELECT * FROM musician WHERE musician_age=17),
     musician_18 AS(SELECT * FROM musician WHERE musician_age=18)
SELECT * FROM musician_16
UNION
SELECT * FROM musician_17
UNION
SELECT * FROM musician_18 ORDER BY musician_age; -- Сформировать таблицы отдельно по возрасту участников группы, а потом из них сформировать одну таблицу где учесть всех музыкантов всех возрастов 7

SELECT musician_age, COUNT(*) FROM musician
GROUP BY musician_age 
HAVING COUNT(*)>1; -- Для статистики определить количество музыкантов одинакового возраста, не учитывая уникальных музыкантов (которые единственные в своем роде имеют такой возраст) 8

