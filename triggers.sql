﻿--Триггер который устанавливает ограничение ссылочной целостности (заменяет ограничение NOT NULL)
CREATE FUNCTION null_negative_instrument_check() RETURNS trigger AS $null_negative_instrument_check$
	BEGIN
		IF NEW.instrument_name IS NULL THEN
			RAISE EXCEPTION 'MUSICIAN INSTRUMENT CANNOT BE NAMELESS';
		END IF;

		IF NEW.instrument_producer_firm IS NULL THEN
			RAISE EXCEPTION 'PRODUCER_FIRM MUST HAVE IT''S NAME';
		END IF;

		IF NEW.instrument_price<=0 THEN
			RAISE EXCEPTION 'INSTRUMENT PRICE CANNOT BE NEGATIVE OR EQUAL ZERO';
		END IF;

		RETURN NEW;
	END;
$null_negative_instrument_check$ LANGUAGE plpgsql; 

CREATE TRIGGER null_negative_instrument_check BEFORE INSERT OR UPDATE ON instrument
	FOR EACH ROW EXECUTE PROCEDURE null_negative_instrument_check();

-- Триггер, который затрагивает несколько записей из разных таблиц. В данном случае идет проверка, чтобы вокалистом группы был именно музыкант-вокалист.
-- Тоесть, чтобы профессиональные навыки музыкантов совпадали с профессиональными задачами в группе.
CREATE FUNCTION role_in_group_checker() RETURNS trigger AS $role_in_group_checker$
	BEGIN
		IF(SELECT musician_role_in_group FROM musician WHERE musician_id=NEW.group_vocalist_id)!='vocalist' THEN
			RAISE EXCEPTION 'YOUR VOCALIST IS NOT A VOCALIST';
		END IF;

		IF(SELECT musician_role_in_group FROM musician WHERE musician_id=NEW.group_lead_guitarist_id)!='lead_guitarist' THEN
			RAISE EXCEPTION 'YOUR LEAD_GUITARIST IS NOT A LEAD_GUITARIST';
		END IF;

		IF(SELECT musician_role_in_group FROM musician WHERE musician_id=NEW.group_rhytm_guitarist_id)!='rhytm_guitarist' THEN
			RAISE EXCEPTION 'YOUR RHYTM_GUITARIST IS NOT A RHYTM GUITARIST';
		END IF;

		IF(SELECT musician_role_in_group FROM musician WHERE musician_id=NEW.group_bass_guitarist_id)!='bass_guitarist' THEN
			RAISE EXCEPTION 'YOUR BASS GUITARIST IS NOT A BASS_GUITARIST';
		END IF;

		IF(SELECT musician_role_in_group FROM musician WHERE musician_id=NEW.group_drummer_id)!='drummer' THEN
			RAISE EXCEPTION 'YOUR DRUMMER IS NOT A DRUMMER';
		END IF;

		RETURN NEW;
	END;
$role_in_group_checker$ LANGUAGE plpgsql;

CREATE TRIGGER role_in_group_checker BEFORE INSERT OR UPDATE ON band
	FOR EACH ROW EXECUTE PROCEDURE role_in_group_checker();

--Триггер автоматического обновления полей. При изменении группой лейбла изменяется количество групп на лейбле.
CREATE FUNCTION label_counter() RETURNS trigger AS $label_counter$

	DECLARE
		label_changes SMALLINT;
		label_changes_2 SMALLINT;

	BEGIN 
		IF TG_OP='INSERT' THEN
			SELECT group_quantity FROM music_label INTO label_changes WHERE label_id=NEW.group_label_id;
			label_changes:=label_changes+1;
			UPDATE music_label SET group_quantity=label_changes WHERE label_id=NEW.group_label_id;
		RETURN NEW;

		ELSIF TG_OP='DELETE' THEN
			SELECT group_quantity FROM music_label INTO label_changes WHERE label_id=OLD.group_label_id;
			label_changes:=label_changes-1;
			UPDATE music_label SET group_quantity=label_changes WHERE label_id=OLD.group_label_id;
		RETURN OLD;

		ELSIF TG_OP='UPDATE' THEN
			SELECT group_quantity FROM music_label INTO label_changes WHERE label_id=OLD.group_label_id;
			label_changes:=label_changes-1;
			UPDATE music_label SET group_quantity=label_changes WHERE label_id=OLD.group_label_id;
			SELECT group_quantity FROM music_label INTO label_changes_2 WHERE label_id=NEW.group_label_id;
			label_changes_2:=label_changes_2+1;
			UPDATE music_label SET group_quantity=label_changes_2 WHERE label_id=NEW.group_label_id;
		RETURN NEW;
		END IF;
	END;
$label_counter$ LANGUAGE plpgsql;

CREATE TRIGGER label_counter BEFORE INSERT OR UPDATE OR DELETE ON band
	FOR EACH ROW EXECUTE PROCEDURE label_counter();

--Триггер журналирования изменения записей в таблице tour
CREATE TABLE tour_log(
user_name VARCHAR 		NOT NULL,
changing_time timestamp 	NOT NULL,
activity VARCHAR 		NOT NULL,
tour_id INTEGER 		NOT NULL,
tour_name VARCHAR(19) 		NOT NULL,		
tour_start_date DATE		NOT NULL,
tour_end_date DATE		NOT NULL,
tour_group_id INTEGER		NOT NULL);

CREATE FUNCTION tour_logging() RETURNS trigger AS $tour_logging$
BEGIN
	IF(TG_OP='INSERT' OR TG_OP='UPDATE') THEN
		INSERT INTO tour_log(user_name, changing_time, activity, tour_id, tour_name, tour_start_date, tour_end_date, tour_group_id)
		VALUES (user, now(), TG_OP, NEW.tour_id, NEW.tour_name, NEW.tour_start_date, NEW.tour_end_date, NEW.tour_group_id);
		RETURN NEW;
	ELSIF(TG_OP='DELETE') THEN
		INSERT INTO tour_log(user_name, changing_time, activity, tour_id, tour_name, tour_start_date, tour_end_date, tour_group_id)
		VALUES (user, now(), TG_OP, OLD.tour_id, OLD.tour_name, OLD.tour_start_date, OLD.tour_end_date, OLD.tour_group_id);
		RETURN OLD;
	END IF;
END;
$tour_logging$ LANGUAGE plpgsql;

CREATE TRIGGER tour_logging AFTER INSERT OR UPDATE OR DELETE ON tour
		FOR EACH ROW EXECUTE PROCEDURE tour_logging();

