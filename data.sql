﻿INSERT INTO instrument(instrument_name, instrument_producer_firm, instrument_price) VALUES
('Fender Jazz Bass','Fender',9000),
('Zildjan Drum Kit', 'Zildjan', 20000),
('Shure-SM58','Shure',1200),
('Jackson San Dimas','Jackson', 7000),
('Gibson Les Paul Standard','Gibson', 24000);

INSERT INTO musician(musician_name, musician_surname, musician_age, musician_role_in_group,musician_instrument_id) VALUES
('Sergey','Denisov', 18, 'vocalist',3),
('Andrew','Shilenko', 18, 'drummer',2),
('Sergey','Sek', 16, 'rhytm_guitarist',5),
('Kyrychenko','Ivan', 17, 'lead_guitarist',4),
('Antony','Aksenov', 18, 'bass_guitarist',1);

INSERT INTO music_label(label_name, group_quantity) VALUES
('RISE RECORDS',0),
('HOMESTEAD RECORDS',0),
('EPITAPH RECORDS',0);

INSERT INTO band(group_name, group_genre, group_label_id, group_vocalist_id, group_lead_guitarist_id,
group_rhytm_guitarist_id, group_bass_guitarist_id, group_drummer_id) VALUES
('While Ashes Fade','Melodic Metalcore',1,1,4,3,5,2);

INSERT INTO album(album_name,album_release_date,track_quantity,album_group_id) VALUES
('Ashes','25/07/2013',7,1);

INSERT INTO tour(tour_name,tour_start_date,tour_end_date,tour_group_id) VALUES
('Defiler_support','10/05/2013','12/05/2013',1);

INSERT INTO listeners(listener_quantity,average_age,listener_group_id) VALUES
(336,18,1);

INSERT INTO clip(clip_song, clip_release_date,clip_group_id) VALUES
('Mediocrity','08/09/2013',1);