﻿CREATE TABLE instrument(
instrument_id SERIAL PRIMARY KEY,
instrument_name VARCHAR(29)		NOT NULL,		--это поле не может быть пустым 1
instrument_producer_firm VARCHAR(9)	NOT NULL,		--это поле не может быть пустым 2
instrument_price SMALLINT		CHECK(instrument_price>0)); --инструмент не может иметь отрицательную стоимость 3

CREATE TYPE role_in_group AS ENUM('vocalist','lead_guitarist','rhytm_guitarist','bass_guitarist','drummer');

CREATE TABLE musician(
musician_id SERIAL PRIMARY KEY,
musician_name VARCHAR(19)		NOT NULL,		--это поле не может быть пустым 4 
musician_surname VARCHAR(19)		NOT NULL,		--это поле не может быть пустым 5
musician_role_in_group role_in_group	NOT NULL,		--это поле не может быть пустым 6 
musician_age 				SMALLINT	CHECK(musician_age>=16), --в группу не берут до 16 лет 19
musician_instrument_id INTEGER		REFERENCES instrument(instrument_id));

CREATE TABLE music_label(
label_id SERIAL PRIMARY KEY,
group_quantity SMALLINT		CHECK(group_quantity>=0),        --на лейбле не может быть отрицательное количество групп 20
label_name VARCHAR(19)		NOT NULL);			--это поле не может быть пустым 7

CREATE TABLE band(
group_id SERIAL PRIMARY KEY,
group_name VARCHAR(19)		UNIQUE,				--имя группы должно быть уникальным 8
group_genre VARCHAR(19)		NOT NULL,			--это поле не может быть пустым 9
group_label_id INTEGER		REFERENCES music_label(label_id),
group_vocalist_id INTEGER		REFERENCES musician(musician_id),
group_lead_guitarist_id INTEGER		REFERENCES musician(musician_id),
group_rhytm_guitarist_id INTEGER	REFERENCES musician(musician_id),
group_bass_guitarist_id INTEGER		REFERENCES musician(musician_id),
group_drummer_id INTEGER		REFERENCES musician(musician_id));

CREATE TABLE album(
album_id SERIAL PRIMARY KEY,
album_name VARCHAR(19)		NOT NULL,			--это поле не может быть пустым 10
album_release_date DATE		NOT NULL,			--это поле не может быть пустым 10
track_quantity SMALLINT		CHECK(track_quantity BETWEEN 0 AND 20),	-- нельзя вылжить альбом с отрицательным количеством треков и никто 11
									-- не записывает альбомы в которых больше 20 треков
album_group_id INTEGER		REFERENCES band(group_id));

CREATE TABLE tour(
tour_id SERIAL PRIMARY KEY,
tour_name VARCHAR(19)		UNIQUE,				--название тура должно быть уникальным 12
tour_start_date DATE		CHECK(tour_start_date>CURRENT_DATE),	--нельзя обьявить турне, которое начинается в прошлом 13
tour_end_date DATE		CHECK(tour_end_date>tour_start_date),	--нельзя закончить тур раньше, чем он начинается 14
tour_group_id INTEGER		REFERENCES band(group_id));

CREATE TABLE listeners(
listener_stats SERIAL PRIMARY KEY,
listener_quantity INTEGER	CHECK(listener_quantity>=0),	--нельзя иметь отрицательное количество слушателей 15
average_age SMALLINT		CHECK(average_age>0),		--нельзя иметь отрицательный возраст 16
listener_group_id INTEGER	REFERENCES band(group_id));

CREATE TABLE clip(
clip_id SERIAL PRIMARY KEY,
clip_group_id INTEGER		REFERENCES band(group_id),
clip_song VARCHAR(19)		NOT NULL,			--это поле не может быть пустым 17
clip_release_date DATE		NOT NULL);			--это поле не может быть пустым 18

