﻿EXPLAIN ANALYZE WITH musician_16 AS(SELECT * FROM musician WHERE musician_age=16),
     musician_17 AS(SELECT * FROM musician WHERE musician_age=17),
     musician_18 AS(SELECT * FROM musician WHERE musician_age=18)
SELECT * FROM musician_16
UNION
SELECT * FROM musician_17
UNION
SELECT * FROM musician_18 ORDER BY musician_age;

EXPLAIN ANALYZE SELECT musician_name, musician_surname, instrument_name, instrument_producer_firm, instrument_price FROM musician
INNER JOIN instrument ON musician.musician_instrument_id=instrument.instrument_id WHERE instrument_price>3000 ORDER BY instrument_price DESC;

CREATE INDEX musician_age_index ON musician(musician_age) WHERE musician_age BETWEEN 16 AND 18;

EXPLAIN ANALYZE WITH musician_16 AS(SELECT * FROM musician WHERE musician_age=16),
     musician_17 AS(SELECT * FROM musician WHERE musician_age=17),
     musician_18 AS(SELECT * FROM musician WHERE musician_age=18)
SELECT * FROM musician_16
UNION
SELECT * FROM musician_17
UNION
SELECT * FROM musician_18 ORDER BY musician_age;

CREATE INDEX instrument_price_index ON instrument(instrument_price);

EXPLAIN ANALYZE SELECT musician_name, musician_surname, instrument_name, instrument_producer_firm, instrument_price FROM musician
INNER JOIN instrument ON musician.musician_instrument_id=instrument.instrument_id WHERE instrument_price>3000 ORDER BY instrument_price DESC;