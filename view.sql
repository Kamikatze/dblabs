﻿CREATE VIEW musicians_and_instruments
	AS SELECT a.musician_id, a.musician_name, a.musician_surname, a.musician_role_in_group, a.musician_age, 
		b.instrument_id, b.instrument_name, b.instrument_producer_firm, b.instrument_price
	FROM musician a, instrument b
	WHERE a.musician_instrument_id=b.instrument_id
	AND instrument_price>=10000;

CREATE FUNCTION insert_update_delete_view() RETURNS trigger AS $insert_update_delete_view$
BEGIN
	IF TG_OP='INSERT' THEN
		INSERT INTO musician(musician_name, musician_surname, musician_role_in_group, musician_age) VALUES
			(NEW.musician_name, NEW.musician_surname, NEW.musician_role_in_group, NEW.musician_age);
		INSERT INTO instrument(instrument_name, instrument_producer_firm, instrument_price) VALUES
			(NEW.instrument_name, NEW.instrument_producer_firm, NEW.instrument_price);
	RETURN NEW;

	ELSIF TG_OP='UPDATE' THEN
		UPDATE musician SET musician_name=NEW.musician_name, musician_surname=NEW.musician_surname, 
				musician_role_in_group=NEW.musician_role_in_group, musician_age=NEW.musician_age
		WHERE musician_name=OLD.musician_name;

		UPDATE instrument SET instrument_name=NEW.instrument_name, instrument_producer_firm=NEW.instrument_producer_firm,
				instrument_price=NEW.instrument_price
		WHERE instrument_name=OLD.instrument_name;
	RETURN NEW;

	ELSIF TG_OP='DELETE' THEN
		DELETE FROM musician WHERE musician_name=OLD.musician_name;
		DELETE FROM instrument WHERE instrument_name=OLD.instrument_name;
	RETURN OLD;
	END IF;
END;
$insert_update_delete_view$ LANGUAGE plpgsql;

CREATE TRIGGER insert_update_delete_view 
INSTEAD OF INSERT OR UPDATE OR DELETE ON musicians_and_instruments
FOR EACH ROW EXECUTE PROCEDURE insert_update_delete_view();

CREATE RULE musicians_and_instruments_insert AS ON INSERT TO musicians_and_instruments
DO INSTEAD(
		INSERT INTO musician(musician_name, musician_surname, musician_role_in_group, musician_age) VALUES
			(NEW.musician_name, NEW.musician_surname, NEW.musician_role_in_group, NEW.musician_age);
		INSERT INTO instrument(instrument_name, instrument_producer_firm, instrument_price) VALUES
			(NEW.instrument_name, NEW.instrument_producer_firm, NEW.instrument_price);
);

CREATE RULE musicians_and_instrument_update AS ON UPDATE TO musicians_and_instruments
DO INSTEAD(
		UPDATE musician SET musician_name=NEW.musician_name, musician_surname=NEW.musician_surname, 
				musician_role_in_group=NEW.musician_role_in_group, musician_age=NEW.musician_age
		WHERE musician_name=OLD.musician_name;

		UPDATE instrument SET instrument_name=NEW.instrument_name, instrument_producer_firm=NEW.instrument_producer_firm,
				instrument_price=NEW.instrument_price
		WHERE instrument_name=OLD.instrument_name;
);

CREATE RULE musicians_and_instruments_delete AS ON DELETE TO musicians_and_instruments
DO INSTEAD(
		DELETE FROM musician WHERE musician_name=OLD.musician_name;
		DELETE FROM instrument WHERE instrument_name=OLD.instrument_name;
);